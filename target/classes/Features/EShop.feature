@Shopping
Feature: Search and add a voucher in the shopping cart

  Background: Enter to the shopping page
    Given User in Shopping page

  @Shopping1
  Scenario: Search for a voucher
    When Category is selected
    And price range is selected
    And product name is filled in search bar
    Then The voucher is displayed

  @Shopping2
  Scenario: Add a voucher in the cart
    When The voucher is added to the cart
    And Validate order is clicked
    And All the fields are filled
    And The user is returned in the shopping page
    And View Cart button is clicked
    And The voucher is removed from the cart
    And Return to shop button is clicked
    Then User is in the shopping page
    And The cart is empty
