package ExpertTestProject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EShopPageObject {

	WebDriver driver;

	public EShopPageObject(WebDriver driver) {

		this.driver = driver;
		PageFactory.initElements(driver, this);
	}


	@FindBy(xpath = "//a[normalize-space()='Voucher de Certification']")
	private WebElement voucherCertification;
	
	@FindBy(id = "menu-item-2946")
	private WebElement eboutiquePage;


	@FindBy(xpath = "//div[@class='price_slider ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content']//span[2]")
	private WebElement rightSlider;

	@FindBy(xpath = "//button[@class='button']")
	private WebElement filterButton;

	@FindBy(id = "woocommerce-product-search-field-0")
	private WebElement searchField;

	@FindBy(xpath = "//button[@value='Recherche']")
	private WebElement searchButton;

	@FindBy(linkText = "Voucher ISTQB® – TESTEUR AGILE")
	private WebElement agileTesteurVoucher;

	@FindBy(linkText = "Voucher de certification ISTQB Foundation")
	private WebElement ISTQBFoundation;

	@FindBy(xpath = "//a[@aria-label='Ajouter “Voucher de certification ISTQB Foundation” à votre panier']")
	private WebElement addISTQBFoucndationToCard;

	@FindBy(xpath = "//a[@class='checkout-button button alt wc-forward']")
	private WebElement orderButton;

	@FindBy(xpath = "//div[@class='nicescroll-cursors']")
	private WebElement scrollCursor;

	@FindBy(id = "billing_first_name")
	private WebElement nameField;

	@FindBy(id = "billing_last_name")
	private WebElement lastNameField;

	@FindBy(id = "billing_state")
	private WebElement stateField;

	@FindBy(id = "billing_address_1")
	private WebElement addressField;

	@FindBy(id = "billing_city")
	private WebElement cityField;

	@FindBy(id = "billing_postcode")
	private WebElement postcodeField;

	@FindBy(id = "billing_phone")
	private WebElement phoneField;

	@FindBy(id = "billing_email")
	private WebElement emailField;

	@FindBy(id = "createaccount")
	private WebElement createaccountCheckbox;

	@FindBy(id = "payment_method_bacs")
	private WebElement payementBancRadio;

	@FindBy(xpath = "//a[@class='button_cart']")
	private WebElement cartButton;

	@FindBy(xpath = "//a[@title='Enlever cet élément']")
	private WebElement deleteOrderIcon;

	@FindBy(xpath = "//span[contains(text(),'Return To Shop')]")
	private WebElement returnToShopButton;

	@FindBy(xpath = "//li[@class='empty']")
	private WebElement cartIsEmpty;

	public WebElement getEboutiquePage() {

		return eboutiquePage;
	}

	public WebElement getVoucherCertification() {

		return voucherCertification;
	}

	public WebElement getRightSlider() {

		return rightSlider;
	}

	public WebElement getFilterButton() {

		return filterButton;
	}

	public WebElement getSearchField() {

		return searchField;
	}

	public WebElement getSearchButton() {

		return searchButton;
	}

	public WebElement getAgileTesteurVoucher() {

		return agileTesteurVoucher;
	}

	public WebElement getISTQBFoundation() {
		return ISTQBFoundation;
	}

	public WebElement getAddISTQBFoucndationToCard() {
		return addISTQBFoucndationToCard;
	}

	public WebElement getOrderButton() {
		return orderButton;
	}

	public WebElement getScrollCursor() {
		return scrollCursor;
	}

	public WebElement getNameField() {
		return nameField;
	}

	public WebElement getLastNameField() {
		return lastNameField;
	}

	public WebElement getStateField() {
		return stateField;
	}

	public WebElement getAddressField() {
		return addressField;
	}

	public WebElement getCityField() {
		return cityField;
	}

	public WebElement getPostcodeField() {
		return postcodeField;
	}

	public WebElement getPhoneField() {
		return phoneField;
	}

	public WebElement getEmailField() {
		return emailField;
	}

	public WebElement getCreateaccountCheckbox() {
		return createaccountCheckbox;
	}

	public WebElement getPayementBancRadio() {
		return payementBancRadio;
	}

	public WebElement getCartButton() {
		return cartButton;
	}

	public WebElement getDeleteOrderIcon() {
		return deleteOrderIcon;
	}

	public WebElement getReturnToShopButton() {
		return returnToShopButton;
	}

	public WebElement getCartIsEmpty() {
		return cartIsEmpty;
	}

}
