package ExpertTestProject;

import java.time.Duration;

import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.java.After;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class EShopStepDefinition {
	public String baseUrl = "https://www.expertest.tn/";
	WebDriver driver;

	EShopPageObject PageObjectEboutique;

	@After

	public void tearDown() {

		driver.quit();
	}

	@Given("User in Shopping page")
	public void User_in_Shopping_page() {

		WebDriverManager.chromedriver().setup();

		// DesiredCapabilities capabilities = new DesiredCapabilities();
		ChromeOptions options = new ChromeOptions();
		// capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200", "--ignore-certificate-errors",
				"--disable-extensions", "--no-sandbox", "--disable-dev-shm-usage");
		driver = new ChromeDriver(options);

		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(60));
		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(60));
		driver.manage().window().maximize();
		driver.get(baseUrl);

		PageObjectEboutique = new EShopPageObject(driver);
		PageObjectEboutique.getEboutiquePage().click();
	}

	@When("Category is selected")
	public void selectCategory() {
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		jsExecutor.executeScript("window.scrollBy(0,350)", "");

		PageObjectEboutique.getVoucherCertification().click();
	}

	@And("price range is selected")
	public void selectPrice() {
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		jsExecutor.executeScript("window.scrollBy(0,350)", "");
		WebElement rightSlider = PageObjectEboutique.getRightSlider();
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
		wait.until(ExpectedConditions.elementToBeClickable(rightSlider));
		int xwidth = rightSlider.getSize().width;
		int yoffset = rightSlider.getSize().height;
		Actions action = new Actions(driver);
		action.dragAndDropBy(rightSlider, xwidth - 120, yoffset);
		action.build().perform();
		PageObjectEboutique.getFilterButton().click();
	}

	@And("product name is filled in search bar")
	public void selectProdcutName() {

		PageObjectEboutique.getSearchField().clear();
		PageObjectEboutique.getSearchField().sendKeys("ISTQB");
		PageObjectEboutique.getSearchButton().click();
	}

	@Then("The voucher is displayed")
	public void voucherExist() {
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		jsExecutor.executeScript("window.scrollBy(0,550)", "");
		PageObjectEboutique.getAgileTesteurVoucher().isDisplayed();
	}

	@When("The voucher is added to the cart")
	public void addTocart() {
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		jsExecutor.executeScript("window.scrollBy(0,350)", "");
		PageObjectEboutique.getAddISTQBFoucndationToCard().click();
	}

	@And("Validate order is clicked")
	public void orderClick() {
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		jsExecutor.executeScript("scroll(0, -350);");
		PageObjectEboutique.getOrderButton().click();
	}

	@And("All the fields are filled")
	public void fillOrder() {
		PageObjectEboutique.getNameField().sendKeys("Aymen");
		PageObjectEboutique.getLastNameField().sendKeys("Nafaa");
		PageObjectEboutique.getStateField().clear();
		PageObjectEboutique.getStateField().sendKeys("Ariana");
		PageObjectEboutique.getAddressField().clear();
		PageObjectEboutique.getAddressField().sendKeys("45 Tunisie Rue Mech 3aref");
		PageObjectEboutique.getCityField().sendKeys("Ariana Soghra");
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		jsExecutor.executeScript("window.scrollBy(0,350)", "");
		PageObjectEboutique.getPostcodeField().sendKeys("5699");
		PageObjectEboutique.getPhoneField().sendKeys("+21654555454");
		PageObjectEboutique.getEmailField().sendKeys("abn@gmail.com");
		jsExecutor.executeScript("window.scrollBy(0,350)", "");
		PageObjectEboutique.getCreateaccountCheckbox().click();
		jsExecutor.executeScript("window.scrollBy(0,350)", "");
		PageObjectEboutique.getPayementBancRadio().click();

	}

	@And("The user is returned in the shopping page")
	public void backToShopping() {
		driver.navigate().back();
		driver.navigate().back();
		driver.navigate().refresh();
	}

	@And("View Cart button is clicked")
	public void viewCart() {
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		jsExecutor.executeScript("window.scrollBy(0,-550)", "");
		PageObjectEboutique.getCartButton().click();
	}

	@And("The voucher is removed from the cart")
	public void removeVoucher() {
		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		jsExecutor.executeScript("window.scrollBy(0,350)", "");
		PageObjectEboutique.getDeleteOrderIcon().click();
	}

	@And("Return to shop button is clicked")
	public void returnToShopping() {
		PageObjectEboutique.getReturnToShopButton().click();
	}

	@Then("User is in the shopping page")
	public void shoppingVerification() {
		Assert.assertEquals(driver.getCurrentUrl(), "https://www.expertest.tn/boutique/");
	}

	@And("The cart is empty")
	public void emptyCart() {
		PageObjectEboutique.getCartIsEmpty().isDisplayed();
	}

}