package ExpertTestProject;

import org.junit.runner.RunWith;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="ressources/Features/EShop.feature",tags= ("@Shopping"), 
plugin={"pretty","html:reports/HtmlReports/report" ,"json:reports/HtmlReports/cucumber.json",
        "junit:reports/junitReports/cucumber.xml"})

public class TestRunner {

}
